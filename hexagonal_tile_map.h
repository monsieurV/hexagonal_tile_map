#ifndef HEXAGONALHexagonalTileMap_H
#define HEXAGONALHexagonalTileMap_H

#include "scene/2d/node_2d.h"
#include "scene/2d/navigation2d.h"
#include "scene/resources/tile_set.h"
#include "self_list.h"
#include "vset.h"

class HexagonalTileMap : public Node2D {

    OBJ_TYPE( HexagonalTileMap, Node2D );
public:

    enum Mode {
        MODE_HORIZONTAL,
        MODE_VERTICAL
    };

    enum Orientation {
        DEGREE_0,
        DEGREE_60,
        DEGREE_120,
        DEGREE_180,
        DEGREE_240,
        DEGREE_300,
    };

    enum TileOrigin {
        TILE_ORIGIN_TOP_LEFT,
        TILE_ORIGIN_CENTER
    };

private:

    Ref<TileSet> tile_set;
    int height_size;
    int edge_size;
    float cell_scale_y;
    int quadrant_size;
    bool center_x,center_y;
    Mode mode;
    Orientation orientation;
    bool use_kinematic;
    Navigation2D *navigation;
    Color modulate;


    union PosKey {

        struct {
            int16_t x;
            int16_t y;
        };
        uint32_t key;

        //using a more precise comparison so the regions can be sorted later
        bool operator<(const PosKey& p_k) const { return (y==p_k.y) ? x < p_k.x : y < p_k.y;  }

        PosKey(int16_t p_x, int16_t p_y) { x=p_x; y=p_y; }
        PosKey() { x=0; y=0; }

    };


    union Cell {

        struct {
            int32_t id:24;
            int32_t side_id:24;
            bool flip_h:1;
            bool flip_v:1;
            bool transpose:1;
        };

        uint32_t _u32t;
        Cell() { _u32t=0; }
    };


    Map<PosKey,Cell> tile_map;
    struct Quadrant {

        Vector2 pos;
        List<RID> canvas_items;
        RID body;

        SelfList<Quadrant> dirty_list;

        struct NavPoly {
            int id;
            Matrix32 xform;
        };

        struct Occluder {
            RID id;
            Matrix32 xform;
        };


        Map<PosKey,NavPoly> navpoly_ids;
        Map<PosKey,Occluder> occluder_instances;

        VSet<PosKey> cells;

        void operator=(const Quadrant& q) { pos=q.pos; canvas_items=q.canvas_items; body=q.body; cells=q.cells; navpoly_ids=q.navpoly_ids; occluder_instances=q.occluder_instances; }
        Quadrant(const Quadrant& q) : dirty_list(this) { pos=q.pos; canvas_items=q.canvas_items; body=q.body; cells=q.cells; occluder_instances=q.occluder_instances; navpoly_ids=q.navpoly_ids;}
        Quadrant() : dirty_list(this) {}
    };

    Map<PosKey,Quadrant> quadrant_map;

    SelfList<Quadrant>::List dirty_quadrant_list;

    bool pending_update;

    Rect2 rect_cache;
    bool rect_cache_dirty;
    bool quadrant_order_dirty;
    bool y_sort_mode;
    float fp_adjust;
    float friction;
    float bounce;
    uint32_t collision_layer;
    uint32_t collision_mask;

    TileOrigin tile_origin;
    Point2 tile_offset;

    int occluder_light_mask;

    void _fix_cell_transform(Matrix32& xform, const Cell& p_cell, const Vector2 &p_offset, const Size2 &p_sc);

    Map<PosKey,Quadrant>::Element *_create_quadrant(const PosKey& p_qk);
    void _erase_quadrant(Map<PosKey,Quadrant>::Element *Q);
    void _make_quadrant_dirty(Map<PosKey,Quadrant>::Element *Q);
    void _recreate_quadrants();
    void _clear_quadrants();
    void _update_dirty_quadrants();
    void _update_quadrant_space(const RID& p_space);
    void _update_quadrant_transform();
    void _recompute_rect_cache();

    _FORCE_INLINE_ int _get_quadrant_size() const;


    void _set_tile_data(const DVector<int>& p_data);
    DVector<int> _get_tile_data() const;

    _FORCE_INLINE_ Vector2 _map_to_world(int p_x,int p_y,bool p_ignore_orientation=false) const;

    Array get_used_cells() const;

protected:


    void _notification(int p_what);
    static void _bind_methods();

public:

    enum {
        INVALID_CELL=-1
    };


    void set_tileset(const Ref<TileSet>& p_tileset);
    Ref<TileSet> get_tileset() const;

    void set_edge_size(int p_edge_size);
    int get_edge_size() const;

    void set_quadrant_size(int p_size);
    int get_quadrant_size() const;

    void set_modulate(const Color& p_color);
    Color get_modulate() const;

    void set_center_x(bool p_enable);
    bool get_center_x() const;
    void set_center_y(bool p_enable);
    bool get_center_y() const;

    void set_cell(int p_x,int p_y,int p_tile,bool p_flip_x=false,bool p_flip_y=false,bool p_transpose=false);
    int get_cell(int p_x,int p_y) const;
    bool is_cell_x_flipped(int p_x,int p_y) const;
    bool is_cell_y_flipped(int p_x,int p_y) const;
    bool is_cell_transposed(int p_x,int p_y) const;

    void set_cellv(const Vector2& p_pos,int p_tile,bool p_flip_x=false,bool p_flip_y=false,bool p_transpose=false);
    int get_cellv(const Vector2& p_pos) const;

    void set_cell_side(int p_x,int p_y,int p_tile);
    void set_cellv_side(const Vector2& p_pos,int p_tile);
    int get_cell_side(int p_x,int p_y) const;

    void set_height_size(int p_height_size);
    int get_height_size() const;

    float get_cell_scale_y() const;
    void set_cell_scale_y(float p_cell_scale_y);

    Rect2 get_item_rect() const;

    void set_collision_layer(uint32_t p_layer);
    uint32_t get_collision_layer() const;

    void set_collision_mask(uint32_t p_mask);
    uint32_t get_collision_mask() const;

    void set_collision_use_kinematic(bool p_use_kinematic);
    bool get_collision_use_kinematic() const;

    void set_collision_friction(float p_friction);
    float get_collision_friction() const;

    void set_collision_bounce(float p_bounce);
    float get_collision_bounce() const;

    void set_mode(Mode p_mode);
    Mode get_mode() const;

    void set_orientation(Orientation p_orientation);
    Orientation get_orientation() const;

    void set_tile_origin(TileOrigin p_tile_origin);
    TileOrigin get_tile_origin() const;

    void set_tile_offset(const Point2& p_tile_offset);
    Point2 get_tile_offset() const;


    Matrix32 get_cell_transform() const;
    Matrix32 get_rect_part_cell_transform() const;
    Vector2 get_cell_draw_offset() const;

    Vector2 map_to_world(const Vector2& p_pos, bool p_ignore_orientation=false) const;
    Vector2 world_to_map(const Vector2& p_pos, bool p_ignore_orientation=false) const;

    Vector2 get_oriented_coordinate(Vector2 p_coordinate, Orientation p_orientation, bool p_reverse=false) const;

    void set_y_sort_mode(bool p_enable);
    bool is_y_sort_mode_enabled() const;

    void set_occluder_light_mask(int p_mask);
    int get_occluder_light_mask() const;

    virtual void set_light_mask(int p_light_mask);

    void clear();

    HexagonalTileMap();
    ~HexagonalTileMap();
};

VARIANT_ENUM_CAST(HexagonalTileMap::Mode);
VARIANT_ENUM_CAST(HexagonalTileMap::Orientation);
VARIANT_ENUM_CAST(HexagonalTileMap::TileOrigin);

#endif // HEXAGONALHexagonalTileMap_H

