/* register_types.cpp */

#include "register_types.h"
#include "object_type_db.h"
#include "hexagonal_tile_map.h"
#include "hexagonal_tile_map_editor_plugin.h"

void register_hexagonal_tile_map_types() {
    ObjectTypeDB::register_type<HexagonalTileMap>();
#ifdef TOOLS_ENABLED
    EditorPlugins::add_by_type<HexagonalTileMapEditorPlugin>();
#endif
}

void unregister_hexagonal_tile_map_types() {
   //nothing to do here
}
