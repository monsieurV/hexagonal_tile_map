# README #

### What is this repository for? ###

* This repository is a module for [godot engine](https://godotengine.org/) that manage hexagonal tilemap.

### How do I get set up? ###

* make sure you can [compile godot](http://docs.godotengine.org/en/latest/reference/_compiling.html).
* Download hexagonal_tile_map sources in the godot/modules directory.
* You can now re-compile godot source with the hexagonal_tile_map module.