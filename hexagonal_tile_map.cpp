#include "hexagonal_tile_map.h"
#include "io/marshalls.h"
#include "servers/physics_2d_server.h"
#include "method_bind_ext.inc"
#include "os/os.h"

int HexagonalTileMap::_get_quadrant_size() const {

    if (y_sort_mode)
        return 1;
    else
        return quadrant_size;
}

void HexagonalTileMap::_notification(int p_what) {

    switch(p_what) {

        case NOTIFICATION_ENTER_TREE: {

            Node2D *c=this;
            while(c) {

                navigation=c->cast_to<Navigation2D>();
                if (navigation) {
                    break;
                }

                c=c->get_parent()->cast_to<Node2D>();
            }

            pending_update=true;
            _update_dirty_quadrants();
            RID space = get_world_2d()->get_space();
            _update_quadrant_transform();
            _update_quadrant_space(space);


        } break;
        case NOTIFICATION_EXIT_TREE: {

            _update_quadrant_space(RID());
            for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

                Quadrant &q=E->get();
                if (navigation) {
                    for(Map<PosKey,Quadrant::NavPoly>::Element *E=q.navpoly_ids.front();E;E=E->next()) {

                        navigation->navpoly_remove(E->get().id);
                    }
                    q.navpoly_ids.clear();
                }

                for(Map<PosKey,Quadrant::Occluder>::Element *E=q.occluder_instances.front();E;E=E->next()) {
                    VS::get_singleton()->free(E->get().id);
                }
                q.occluder_instances.clear();
            }

            navigation=NULL;


        } break;
        case NOTIFICATION_TRANSFORM_CHANGED: {

            //move stuff
            _update_quadrant_transform();

        } break;
    }
}

void HexagonalTileMap::_update_quadrant_space(const RID& p_space) {

    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Physics2DServer::get_singleton()->body_set_space(q.body,p_space);
    }
}

void HexagonalTileMap::_update_quadrant_transform() {

    if (!is_inside_tree())
        return;

    Matrix32 global_transform = get_global_transform();

    Matrix32 nav_rel;
    if (navigation)
        nav_rel = get_relative_transform_to_parent(navigation);

    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Matrix32 xform;
        xform.set_origin( q.pos );
        xform = global_transform * xform;
        Physics2DServer::get_singleton()->body_set_state(q.body,Physics2DServer::BODY_STATE_TRANSFORM,xform);

        if (navigation) {
            for(Map<PosKey,Quadrant::NavPoly>::Element *E=q.navpoly_ids.front();E;E=E->next()) {

                navigation->navpoly_set_transform(E->get().id,nav_rel * E->get().xform);
            }
        }

        for(Map<PosKey,Quadrant::Occluder>::Element *E=q.occluder_instances.front();E;E=E->next()) {
            VS::get_singleton()->canvas_light_occluder_set_transform(E->get().id,global_transform * E->get().xform);
        }
    }
}

void HexagonalTileMap::set_tileset(const Ref<TileSet>& p_tileset) {

    if (tile_set.is_valid())
        tile_set->disconnect("changed",this,"_recreate_quadrants");

    _clear_quadrants();
    tile_set=p_tileset;

    if (tile_set.is_valid())
        tile_set->connect("changed",this,"_recreate_quadrants");
    else
        clear();

    _recreate_quadrants();
    emit_signal("settings_changed");

}

void HexagonalTileMap::set_edge_size(int p_edge_size) {
    ERR_FAIL_COND(p_edge_size<1);
    edge_size = p_edge_size;

    _recreate_quadrants();
    emit_signal("settings_changed");
}

int HexagonalTileMap::get_edge_size() const {
    return edge_size;
}

Ref<TileSet> HexagonalTileMap::get_tileset() const {

    return tile_set;
}

void HexagonalTileMap::set_quadrant_size(int p_size) {

    ERR_FAIL_COND(p_size<1);

    _clear_quadrants();
    quadrant_size=p_size;
    _recreate_quadrants();
    emit_signal("settings_changed");

}
int HexagonalTileMap::get_quadrant_size() const {

    return quadrant_size;
}

void HexagonalTileMap::set_modulate(const Color& p_color) {

    modulate=p_color;
    _recreate_quadrants();
}

Color HexagonalTileMap::get_modulate() const{

    return modulate;
}

void HexagonalTileMap::set_center_x(bool p_enable) {

    center_x=p_enable;
    _recreate_quadrants();
    emit_signal("settings_changed");


}
bool HexagonalTileMap::get_center_x() const {

    return center_x;
}
void HexagonalTileMap::set_center_y(bool p_enable) {

    center_y=p_enable;
    _recreate_quadrants();
    emit_signal("settings_changed");

}
bool HexagonalTileMap::get_center_y() const {

    return center_y;
}

void HexagonalTileMap::_fix_cell_transform(Matrix32& xform,const Cell& p_cell, const Vector2& p_offset, const Size2 &p_sc) {

    Size2 s=p_sc;
    Vector2 offset = p_offset;

    if (s.y > s.x) {
        if ((p_cell.flip_h && (p_cell.flip_v || p_cell.transpose)) || (p_cell.flip_v && !p_cell.transpose))
            offset.y += s.y - s.x;
    } else if (s.y < s.x) {
        if ((p_cell.flip_v && (p_cell.flip_h || p_cell.transpose)) || (p_cell.flip_h && !p_cell.transpose))
            offset.x += s.x - s.y;
    }

    if (p_cell.transpose) {
        SWAP(xform.elements[0].x, xform.elements[0].y);
        SWAP(xform.elements[1].x, xform.elements[1].y);
        SWAP(offset.x, offset.y);
        SWAP(s.x, s.y);
    }
    if (p_cell.flip_h) {
        xform.elements[0].x=-xform.elements[0].x;
        xform.elements[1].x=-xform.elements[1].x;
        if (tile_origin==TILE_ORIGIN_TOP_LEFT)
            offset.x=s.x-offset.x;
    }
    if (p_cell.flip_v) {
        xform.elements[0].y=-xform.elements[0].y;
        xform.elements[1].y=-xform.elements[1].y;
        if (tile_origin==TILE_ORIGIN_TOP_LEFT)
            offset.y=s.y-offset.y;
    }
    xform.elements[2].x+=offset.x;
    xform.elements[2].y+=offset.y;

}

void HexagonalTileMap::_update_dirty_quadrants() {

    if (!pending_update)
        return;
    if (!is_inside_tree())
        return;
    if (!tile_set.is_valid())
        return;

    VisualServer *vs = VisualServer::get_singleton();
    Physics2DServer *ps = Physics2DServer::get_singleton();
    Vector2 tofs = get_cell_draw_offset();
    Vector2 tcenter = Vector2();//cell_size/2;//tmp !!
    Matrix32 nav_rel;
    if (navigation)
        nav_rel = get_relative_transform_to_parent(navigation);

    Vector2 qofs;

    SceneTree *st=SceneTree::get_singleton();
    Color debug_collision_color;

    bool debug_shapes = st && st->is_debugging_collisions_hint();
    if (debug_shapes) {
        debug_collision_color=st->get_debug_collisions_color();
    }

    while (dirty_quadrant_list.first()) {

        Quadrant &q = *dirty_quadrant_list.first()->self();

        for (List<RID>::Element *E=q.canvas_items.front();E;E=E->next()) {

            vs->free(E->get());
        }

        q.canvas_items.clear();

        ps->body_clear_shapes(q.body);
        int shape_idx=0;

        if (navigation) {
            for(Map<PosKey,Quadrant::NavPoly>::Element *E=q.navpoly_ids.front();E;E=E->next()) {

                navigation->navpoly_remove(E->get().id);
            }
            q.navpoly_ids.clear();
        }

        for(Map<PosKey,Quadrant::Occluder>::Element *E=q.occluder_instances.front();E;E=E->next()) {
            VS::get_singleton()->free(E->get().id);
        }
        q.occluder_instances.clear();
        Ref<CanvasItemMaterial> prev_material;
        RID prev_canvas_item;
        RID prev_debug_canvas_item;

        for(int i=0;i<q.cells.size();i++) {

            Map<PosKey,Cell>::Element *E=tile_map.find( q.cells[i] );
            Cell &c=E->get();
            //moment of truth
            if (!tile_set->has_tile(c.id))
                continue;
            Ref<Texture> tex = tile_set->tile_get_texture(c.id);
            Vector2 tile_ofs = tile_set->tile_get_texture_offset(c.id);

            Vector2 wofs = _map_to_world(E->key().x, E->key().y);
            Vector2 offset = wofs - q.pos + tofs;

            if (!tex.is_valid())
                continue;

            Ref<CanvasItemMaterial> mat = tile_set->tile_get_material(c.id);

            RID canvas_item;
            RID debug_canvas_item;

            if (prev_canvas_item==RID() || prev_material!=mat) {

                canvas_item=vs->canvas_item_create();
                if (mat.is_valid())
                    vs->canvas_item_set_material(canvas_item,mat->get_rid());
                vs->canvas_item_set_parent( canvas_item, get_canvas_item() );
                Matrix32 xform;
                xform.set_origin( q.pos );
                vs->canvas_item_set_transform( canvas_item, xform );
                vs->canvas_item_set_light_mask(canvas_item,get_light_mask());

                q.canvas_items.push_back(canvas_item);

                if (debug_shapes) {

                    debug_canvas_item=vs->canvas_item_create();
                    vs->canvas_item_set_parent( debug_canvas_item, canvas_item );
                    vs->canvas_item_set_z_as_relative_to_parent(debug_canvas_item,false);
                    vs->canvas_item_set_z(debug_canvas_item,VS::CANVAS_ITEM_Z_MAX-1);
                    q.canvas_items.push_back(debug_canvas_item);
                    prev_debug_canvas_item=debug_canvas_item;
                }

                prev_canvas_item=canvas_item;
                prev_material=mat;

            } else {
                canvas_item=prev_canvas_item;
                if (debug_shapes) {
                    debug_canvas_item=prev_debug_canvas_item;
                }
            }



            Rect2 r = tile_set->tile_get_region(c.id);
            Size2 s = tex->get_size();

            if (r==Rect2())
                s = tex->get_size();
            else {
                s = r.size;
                r.pos.x+=fp_adjust;
                r.pos.y+=fp_adjust;
                r.size.x-=fp_adjust*2.0;
                r.size.y-=fp_adjust*2.0;
            }

            Rect2 rect;
            rect.pos=offset.floor();
            rect.size=s;

            if (rect.size.y > rect.size.x) {
                if ((c.flip_h && (c.flip_v || c.transpose)) || (c.flip_v && !c.transpose))
                    tile_ofs.y += rect.size.y - rect.size.x;
            } else if (rect.size.y < rect.size.x) {
                if ((c.flip_v && (c.flip_h || c.transpose)) || (c.flip_h && !c.transpose))
                    tile_ofs.x += rect.size.x - rect.size.y;
            }

        /*	rect.size.x+=fp_adjust;
            rect.size.y+=fp_adjust;*/

            if (c.transpose)
                SWAP(tile_ofs.x, tile_ofs.y);

            if (c.flip_h) {
                rect.size.x=-rect.size.x;
                tile_ofs.x=-tile_ofs.x;
            }
            if (c.flip_v) {
                rect.size.y=-rect.size.y;
                tile_ofs.y=-tile_ofs.y;
            }

            rect.size.y *= cell_scale_y;

            Vector2 center_ofs;

            if (tile_origin==TILE_ORIGIN_TOP_LEFT) {
                rect.pos+=tile_ofs;
            } else if (tile_origin==TILE_ORIGIN_CENTER) {
                rect.pos+=tcenter;

                Vector2 center = (s/2) - tile_ofs;
                center_ofs=tcenter-(s/2);

                if (c.flip_h)
                    rect.pos.x-=s.x-center.x;
                else
                    rect.pos.x-=center.x;

                if (c.flip_v)
                    rect.pos.y-=s.y-center.y;
                else
                    rect.pos.y-=center.y;
            }

            //tmp do not work with flip !!  Usefull ?
            //rect.pos -= tile_offset;

            Ref<Texture> sidetex;
            Rect2 side_rect = Rect2();

            if (tile_set->has_tile(c.side_id)) {
                sidetex = tile_set->tile_get_texture(c.side_id);
                Matrix32 rect_part_cell_transform = get_rect_part_cell_transform();
                switch(mode) {
                    case MODE_HORIZONTAL: {
                        side_rect.pos = rect.pos + rect_part_cell_transform[1];
                    } break;
                    case MODE_VERTICAL: {
                        side_rect.pos = rect.pos + rect_part_cell_transform[1] * 3;
                    } break;
                }

                Size2 s_side = sidetex->get_size();
                side_rect.size=s_side;
                side_rect.size.y *= cell_scale_y / 3;
            }

            if (r==Rect2()) {
                tex->draw_rect(canvas_item,rect,false,modulate,c.transpose);
            } else {
                tex->draw_rect_region(canvas_item,rect,r,modulate,c.transpose);
            }

            if (sidetex.is_valid()) {
                Rect2 r_side = tile_set->tile_get_region(c.side_id);
                if (r_side==Rect2()) {
                    r_side = Rect2(0, 0, sidetex->get_width(), sidetex->get_height());
                }

                r_side.size.y /= 3;
                sidetex->draw_rect_region(canvas_item,side_rect, r_side);
                r_side.pos.y += r_side.size.y;
                side_rect.pos.y += side_rect.size.y;
                Rect2 side_rect_middle = Rect2(side_rect.pos.x, side_rect.pos.y, side_rect.size.x, height_size * (1 - cell_scale_y));

                sidetex->draw_rect_region(canvas_item,side_rect_middle, r_side);

                r_side.pos.y += r_side.size.y;
                side_rect.pos.y += side_rect_middle.size.y;
                sidetex->draw_rect_region(canvas_item,side_rect, r_side);
            }

            Vector< Ref<Shape2D> > shapes = tile_set->tile_get_shapes(c.id);


            for(int i=0;i<shapes.size();i++) {

                Ref<Shape2D> shape = shapes[i];
                if (shape.is_valid()) {

                    Vector2 shape_ofs = tile_set->tile_get_shape_offset(c.id);
                    Matrix32 xform;
                    xform.set_origin(offset.floor());

                    _fix_cell_transform(xform,c,shape_ofs+center_ofs,s);

                    if (debug_canvas_item) {
                        vs->canvas_item_add_set_transform(debug_canvas_item,xform);
                        shape->draw(debug_canvas_item,debug_collision_color);

                    }
                    ps->body_add_shape(q.body,shape->get_rid(),xform);
                    ps->body_set_shape_metadata(q.body,shape_idx++,Vector2(E->key().x,E->key().y));

                }
            }

            if (debug_canvas_item) {
                vs->canvas_item_add_set_transform(debug_canvas_item,Matrix32());
            }

            if (navigation) {
                Ref<NavigationPolygon> navpoly = tile_set->tile_get_navigation_polygon(c.id);
                if (navpoly.is_valid()) {
                    Vector2 npoly_ofs = tile_set->tile_get_navigation_polygon_offset(c.id);
                    Matrix32 xform;
                    xform.set_origin(offset.floor()+q.pos);
                    _fix_cell_transform(xform,c,npoly_ofs+center_ofs,s);


                    int pid = navigation->navpoly_create(navpoly,nav_rel * xform);

                    Quadrant::NavPoly np;
                    np.id=pid;
                    np.xform=xform;
                    q.navpoly_ids[E->key()]=np;
                }
            }


            Ref<OccluderPolygon2D> occluder=tile_set->tile_get_light_occluder(c.id);
            if (occluder.is_valid()) {

                Vector2 occluder_ofs = tile_set->tile_get_occluder_offset(c.id);
                Matrix32 xform;
                xform.set_origin(offset.floor()+q.pos);
                _fix_cell_transform(xform,c,occluder_ofs+center_ofs,s);

                RID orid = VS::get_singleton()->canvas_light_occluder_create();
                VS::get_singleton()->canvas_light_occluder_set_transform(orid,get_global_transform() * xform);
                VS::get_singleton()->canvas_light_occluder_set_polygon(orid,occluder->get_rid());
                VS::get_singleton()->canvas_light_occluder_attach_to_canvas(orid,get_canvas());
                VS::get_singleton()->canvas_light_occluder_set_light_mask(orid,occluder_light_mask);
                Quadrant::Occluder oc;
                oc.xform=xform;
                oc.id=orid;
                q.occluder_instances[E->key()]=oc;
            }
        }

        dirty_quadrant_list.remove( dirty_quadrant_list.first() );
        quadrant_order_dirty=true;
    }



    pending_update=false;

    if (quadrant_order_dirty) {

        for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

            Quadrant &q=E->get();
            for (List<RID>::Element *E=q.canvas_items.front();E;E=E->next()) {

                VS::get_singleton()->canvas_item_raise(E->get());
            }
        }

        quadrant_order_dirty=false;
    }

    for(int i=0;i<get_child_count();i++) {

        CanvasItem *c=get_child(i)->cast_to<CanvasItem>();

        if (c)
            VS::get_singleton()->canvas_item_raise(c->get_canvas_item());
    }

    _recompute_rect_cache();

}

void HexagonalTileMap::_recompute_rect_cache() {


#ifdef DEBUG_ENABLED

    if (!rect_cache_dirty)
        return;

    Rect2 r_total;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {


        Rect2 r;
        r.pos=_map_to_world(E->key().x*_get_quadrant_size(), E->key().y*_get_quadrant_size());
        r.expand_to( _map_to_world(E->key().x*_get_quadrant_size()+_get_quadrant_size(), E->key().y*_get_quadrant_size()) );
        r.expand_to( _map_to_world(E->key().x*_get_quadrant_size()+_get_quadrant_size(), E->key().y*_get_quadrant_size()+_get_quadrant_size()) );
        r.expand_to( _map_to_world(E->key().x*_get_quadrant_size(), E->key().y*_get_quadrant_size()+_get_quadrant_size()) );
        if (E==quadrant_map.front())
            r_total=r;
        else
            r_total=r_total.merge(r);

    }

    if (r_total==Rect2()) {
        rect_cache=Rect2(-10,-10,20,20);
    } else {
        //rect_cache=r_total.grow(MAX(cell_size.x,cell_size.y)*_get_quadrant_size());//tmp ???? what for ???
        rect_cache=r_total.grow(edge_size*_get_quadrant_size());//tmp
    }

    item_rect_changed();

    rect_cache_dirty=false;
#endif


}

Map<HexagonalTileMap::PosKey,HexagonalTileMap::Quadrant>::Element *HexagonalTileMap::_create_quadrant(const PosKey& p_qk) {

    Matrix32 xform;
    //xform.set_origin(Point2(p_qk.x,p_qk.y)*cell_size*quadrant_size);
    Quadrant q;
    q.pos = _map_to_world(p_qk.x*_get_quadrant_size(),p_qk.y*_get_quadrant_size());
    q.pos+=get_cell_draw_offset();
    if (tile_origin==TILE_ORIGIN_CENTER)
        q.pos+=Vector2();//cell_size/2;//tmp

    xform.set_origin( q.pos );
//	q.canvas_item = VisualServer::get_singleton()->canvas_item_create();
    q.body=Physics2DServer::get_singleton()->body_create(use_kinematic?Physics2DServer::BODY_MODE_KINEMATIC:Physics2DServer::BODY_MODE_STATIC);
    Physics2DServer::get_singleton()->body_attach_object_instance_ID(q.body,get_instance_ID());
    Physics2DServer::get_singleton()->body_set_layer_mask(q.body,collision_layer);
    Physics2DServer::get_singleton()->body_set_collision_mask(q.body,collision_mask);
    Physics2DServer::get_singleton()->body_set_param(q.body,Physics2DServer::BODY_PARAM_FRICTION,friction);
    Physics2DServer::get_singleton()->body_set_param(q.body,Physics2DServer::BODY_PARAM_BOUNCE,bounce);

    if (is_inside_tree()) {
        xform = get_global_transform() * xform;
        RID space = get_world_2d()->get_space();
        Physics2DServer::get_singleton()->body_set_space(q.body,space);
    }

    Physics2DServer::get_singleton()->body_set_state(q.body,Physics2DServer::BODY_STATE_TRANSFORM,xform);

    rect_cache_dirty=true;
    quadrant_order_dirty=true;
    return quadrant_map.insert(p_qk,q);
}

void HexagonalTileMap::_erase_quadrant(Map<PosKey,Quadrant>::Element *Q) {

    Quadrant &q=Q->get();
    Physics2DServer::get_singleton()->free(q.body);
    for (List<RID>::Element *E=q.canvas_items.front();E;E=E->next()) {

        VisualServer::get_singleton()->free(E->get());
    }
    q.canvas_items.clear();
    if (q.dirty_list.in_list())
        dirty_quadrant_list.remove(&q.dirty_list);

    if (navigation) {
        for(Map<PosKey,Quadrant::NavPoly>::Element *E=q.navpoly_ids.front();E;E=E->next()) {

            navigation->navpoly_remove(E->get().id);
        }
        q.navpoly_ids.clear();
    }

    for(Map<PosKey,Quadrant::Occluder>::Element *E=q.occluder_instances.front();E;E=E->next()) {
        VS::get_singleton()->free(E->get().id);
    }
    q.occluder_instances.clear();

    quadrant_map.erase(Q);
    rect_cache_dirty=true;
}

void HexagonalTileMap::_make_quadrant_dirty(Map<PosKey,Quadrant>::Element *Q) {

    Quadrant &q=Q->get();
    if (!q.dirty_list.in_list())
        dirty_quadrant_list.add(&q.dirty_list);

    if (pending_update)
        return;
    pending_update=true;
    if (!is_inside_tree())
        return;
    call_deferred("_update_dirty_quadrants");
}

void HexagonalTileMap::set_cellv(const Vector2& p_pos,int p_tile,bool p_flip_x,bool p_flip_y,bool p_transpose) {

    set_cell(p_pos.x,p_pos.y,p_tile,p_flip_x,p_flip_y,p_transpose);
}

void HexagonalTileMap::set_cell(int p_x,int p_y,int p_tile,bool p_flip_x,bool p_flip_y,bool p_transpose) {//tmp transpose (2mode, tmp add height !!

    PosKey pk(p_x,p_y);
    bool new_cell = false;

    Map<PosKey,Cell>::Element *E=tile_map.find(pk);
    if (!E && p_tile==INVALID_CELL)
        return; //nothing to do

    PosKey qk(p_x/_get_quadrant_size(),p_y/_get_quadrant_size());
    if (p_tile==INVALID_CELL) {
        //erase existing
        tile_map.erase(pk);
        Map<PosKey,Quadrant>::Element *Q = quadrant_map.find(qk);
        ERR_FAIL_COND(!Q);
        Quadrant &q=Q->get();
        q.cells.erase(pk);
        if (q.cells.size()==0)
            _erase_quadrant(Q);
        else
            _make_quadrant_dirty(Q);

        return;
    }

    Map<PosKey,Quadrant>::Element *Q = quadrant_map.find(qk);

    if (!E) {
        E=tile_map.insert(pk,Cell());
        if (!Q) {
            Q=_create_quadrant(qk);
        }
        Quadrant &q=Q->get();
        q.cells.insert(pk);
        new_cell = true;
    } else {
        ERR_FAIL_COND(!Q); // quadrant should exist...

        if (E->get().id==p_tile && E->get().flip_h==p_flip_x && E->get().flip_v==p_flip_y && E->get().transpose==p_transpose)
            return; //nothing changed

    }

    Cell &c = E->get();

    c.id=p_tile;
    c.flip_h=p_flip_x;
    c.flip_v=p_flip_y;
    c.transpose=p_transpose;
    if (new_cell) {
        c.side_id = INVALID_CELL;
    }

    _make_quadrant_dirty(Q);
}

int HexagonalTileMap::get_cellv(const Vector2& p_pos) const {
    return get_cell(p_pos.x,p_pos.y);
}

void HexagonalTileMap::set_cell_side(int p_x,int p_y,int p_tile) {
    PosKey pk(p_x,p_y);

    Map<PosKey,Cell>::Element *E=tile_map.find(pk);
    if (!E && p_tile==INVALID_CELL)
        return; //nothing to do

    PosKey qk(p_x/_get_quadrant_size(),p_y/_get_quadrant_size());
//    if (p_tile==INVALID_CELL || ) {
//        //erase existing
//        /
//        return;
//   }

    Map<PosKey,Quadrant>::Element *Q = quadrant_map.find(qk);

    if (!E) {
        return;//add side only if ground exist
    } else {
        ERR_FAIL_COND(!Q); // quadrant should exist...
    }


    Cell &c = E->get();

    p_tile = (c.id == INVALID_CELL) ? INVALID_CELL : p_tile;

    c.side_id=p_tile;

    _make_quadrant_dirty(Q);
}

int HexagonalTileMap::get_cell_side(int p_x,int p_y) const {

    PosKey pk(p_x,p_y);

    const Map<PosKey,Cell>::Element *E=tile_map.find(pk);

    if (!E)
        return INVALID_CELL;

    return E->get().side_id;

}

void HexagonalTileMap::set_cellv_side(const Vector2& p_pos,int p_tile) {
    set_cell_side(p_pos.x, p_pos.y, p_tile);
}

int HexagonalTileMap::get_height_size() const {
    return height_size;
}

void HexagonalTileMap::set_height_size(int p_height_size) {
    height_size = p_height_size;

    _recreate_quadrants();
    emit_signal("settings_changed");
}

float HexagonalTileMap::get_cell_scale_y() const {
    return cell_scale_y;
}

void HexagonalTileMap::set_cell_scale_y(float p_cell_scale_y) {
    p_cell_scale_y = (p_cell_scale_y <= 0) ? 0.01 : p_cell_scale_y;
    p_cell_scale_y = (p_cell_scale_y > 1) ? 1 : p_cell_scale_y;
    cell_scale_y = p_cell_scale_y;

    _recreate_quadrants();
    emit_signal("settings_changed");
}

int HexagonalTileMap::get_cell(int p_x,int p_y) const {

    PosKey pk(p_x,p_y);

    const Map<PosKey,Cell>::Element *E=tile_map.find(pk);

    if (!E)
        return INVALID_CELL;

    return E->get().id;

}
bool HexagonalTileMap::is_cell_x_flipped(int p_x,int p_y) const {

    PosKey pk(p_x,p_y);

    const Map<PosKey,Cell>::Element *E=tile_map.find(pk);

    if (!E)
        return false;

    return E->get().flip_h;
}
bool HexagonalTileMap::is_cell_y_flipped(int p_x,int p_y) const {

    PosKey pk(p_x,p_y);

    const Map<PosKey,Cell>::Element *E=tile_map.find(pk);

    if (!E)
        return false;

    return E->get().flip_v;
}
bool HexagonalTileMap::is_cell_transposed(int p_x,int p_y) const {

    PosKey pk(p_x,p_y);

    const Map<PosKey,Cell>::Element *E=tile_map.find(pk);

    if (!E)
        return false;

    return E->get().transpose;
}


void HexagonalTileMap::_recreate_quadrants() {

    _clear_quadrants();

    for (Map<PosKey,Cell>::Element *E=tile_map.front();E;E=E->next()) {

        PosKey qk(E->key().x/_get_quadrant_size(),E->key().y/_get_quadrant_size());

        Map<PosKey,Quadrant>::Element *Q=quadrant_map.find(qk);
        if (!Q) {
            Q=_create_quadrant(qk);
            dirty_quadrant_list.add(&Q->get().dirty_list);
        }

        Q->get().cells.insert(E->key());
        _make_quadrant_dirty(Q);
    }



}


void HexagonalTileMap::_clear_quadrants() {

    while (quadrant_map.size()) {
        _erase_quadrant( quadrant_map.front() );
    }
}

void HexagonalTileMap::clear() {

    _clear_quadrants();
    tile_map.clear();
}

void HexagonalTileMap::_set_tile_data(const DVector<int>& p_data) {

    int c=p_data.size();
    DVector<int>::Read r = p_data.read();


    for(int i=0;i<c;i+=3) {

        const uint8_t *ptr=(const uint8_t*)&r[i];
        uint8_t local[12];
        for(int j=0;j<12;j++)
            local[j]=ptr[j];

#ifdef BIG_ENDIAN_ENABLED


        SWAP(local[0],local[3]);
        SWAP(local[1],local[2]);
        SWAP(local[4],local[7]);
        SWAP(local[5],local[6]);
        SWAP(local[8],local[11]);
        SWAP(local[9],local[10]);
#endif

        int16_t x = decode_uint16(&local[0]);
        int16_t y = decode_uint16(&local[2]);
        uint32_t v = decode_uint32(&local[4]);
        bool flip_h = v&(1<<29);
        bool flip_v = v&(1<<30);
        bool transpose = v&(1<<31);
        v&=(1<<29)-1;

        uint32_t vs = decode_uint32(&local[8]);

//		if (x<-20 || y <-20 || x>4000 || y>4000)
//			continue;
        set_cell(x,y,v,flip_h,flip_v,transpose);
        set_cell_side(x, y, vs);

    }

}

DVector<int> HexagonalTileMap::_get_tile_data() const {

    DVector<int> data;
    data.resize(tile_map.size()*3);
    DVector<int>::Write w = data.write();

    int idx=0;
    for(const Map<PosKey,Cell>::Element *E=tile_map.front();E;E=E->next()) {

        uint8_t *ptr = (uint8_t*)&w[idx];
        encode_uint16(E->key().x,&ptr[0]);
        encode_uint16(E->key().y,&ptr[2]);
        uint32_t val = E->get().id;
        if (E->get().flip_h)
            val|=(1<<29);
        if (E->get().flip_v)
            val|=(1<<30);
        if (E->get().transpose)
            val|=(1<<31);

        encode_uint32(val,&ptr[4]);

        uint32_t side_val = E->get().side_id;
        encode_uint32(side_val,&ptr[8]);

        idx+=3;
    }


    w = DVector<int>::Write();

    return data;

}

Rect2 HexagonalTileMap::get_item_rect() const {

    const_cast<HexagonalTileMap*>(this)->_update_dirty_quadrants();
    return rect_cache;
}

void HexagonalTileMap::set_collision_layer(uint32_t p_layer) {

    collision_layer=p_layer;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Physics2DServer::get_singleton()->body_set_layer_mask(q.body,collision_layer);
    }
}

void HexagonalTileMap::set_collision_mask(uint32_t p_mask) {

    collision_mask=p_mask;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Physics2DServer::get_singleton()->body_set_collision_mask(q.body,collision_mask);
    }
}

bool HexagonalTileMap::get_collision_use_kinematic() const{

    return use_kinematic;
}

void HexagonalTileMap::set_collision_use_kinematic(bool p_use_kinematic) {

    _clear_quadrants();
    use_kinematic=p_use_kinematic;
    _recreate_quadrants();
}

void HexagonalTileMap::set_collision_friction(float p_friction) {

    friction=p_friction;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Physics2DServer::get_singleton()->body_set_param(q.body,Physics2DServer::BODY_PARAM_FRICTION,p_friction);
    }

}

float HexagonalTileMap::get_collision_friction() const{

    return friction;
}

void HexagonalTileMap::set_collision_bounce(float p_bounce){

    bounce=p_bounce;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        Quadrant &q=E->get();
        Physics2DServer::get_singleton()->body_set_param(q.body,Physics2DServer::BODY_PARAM_BOUNCE,p_bounce);
    }

}
float HexagonalTileMap::get_collision_bounce() const{

    return bounce;
}


uint32_t HexagonalTileMap::get_collision_layer() const {

    return collision_layer;
}

uint32_t HexagonalTileMap::get_collision_mask() const {

    return collision_mask;
}

void HexagonalTileMap::set_mode(Mode p_mode) {

    _clear_quadrants();
    mode=p_mode;
    _recreate_quadrants();
    emit_signal("settings_changed");
}

HexagonalTileMap::Mode HexagonalTileMap::get_mode() const {
    return mode;
}

void HexagonalTileMap::set_orientation(Orientation p_orientation) {

    _clear_quadrants();
    orientation=p_orientation;
    _recreate_quadrants();
    emit_signal("settings_changed");
}

HexagonalTileMap::Orientation HexagonalTileMap::get_orientation() const {
    return orientation;
}

void HexagonalTileMap::set_tile_origin(TileOrigin p_tile_origin) {

    _clear_quadrants();// tmp usefull ?
    tile_origin=p_tile_origin;
    _recreate_quadrants();
    emit_signal("settings_changed");
}

HexagonalTileMap::TileOrigin HexagonalTileMap::get_tile_origin() const{

    return tile_origin;
}

void HexagonalTileMap::set_tile_offset(const Point2& p_tile_offset) {

    tile_offset=p_tile_offset;
    _recreate_quadrants();
    emit_signal("settings_changed");
}
Point2 HexagonalTileMap::get_tile_offset() const {

    return tile_offset;
}

//tmp manage scale y (or in get_cell_transform ???) !!!
Vector2 HexagonalTileMap::get_cell_draw_offset() const {//tmp usefull for wath ??
    return Vector2();
}

Matrix32 HexagonalTileMap::get_cell_transform() const {
    switch(mode) {
        case MODE_HORIZONTAL: {
            Matrix32 m;
            m[0]=Vector2(1.5*edge_size, edge_size * Math::cos(0.524) * cell_scale_y);
            m[1]=Vector2(0, edge_size * Math::cos(0.524) * 2 * cell_scale_y);
            return m;
        } break;
        case MODE_VERTICAL: {
            Matrix32 m;
            m[0]=Vector2(edge_size * Math::cos(0.524) * 2, 0);
            m[1]=Vector2(edge_size * Math::cos(0.524), edge_size * 1.5 * cell_scale_y);
            return m;

        } break;
    }

    return Matrix32();
}

Matrix32 HexagonalTileMap::get_rect_part_cell_transform() const {
    switch(mode) {
        case MODE_HORIZONTAL: {
            Matrix32 m;
            m[0]=Vector2(0.5*edge_size, 0);
            m[1]=Vector2(0, edge_size * Math::cos(0.524) * cell_scale_y);
            return m;
        } break;
        case MODE_VERTICAL: {
            Matrix32 m;
            m[0]=Vector2(edge_size * Math::cos(0.524), 0);
            m[1]=Vector2(0, edge_size * 0.5 * cell_scale_y);
            return m;

        } break;
    }

    return Matrix32();
}

Vector2 HexagonalTileMap::_map_to_world(int x,int y,bool p_ignore_orientation) const {//tmp ignore offset uselesss ???
    Vector2 ret;
    if (p_ignore_orientation) {
        ret = Vector2(x,y);
    } else {
        ret = get_oriented_coordinate(Vector2(x, y), orientation);
    }

    ret = get_cell_transform().xform(ret);

    return ret;
}

Vector2 HexagonalTileMap::map_to_world(const Vector2& p_pos,bool p_ignore_orientation) const {

    return _map_to_world(p_pos.x,p_pos.y,p_ignore_orientation);
}

Vector2 HexagonalTileMap::world_to_map(const Vector2& p_pos, bool p_ignore_orientation) const {
    Vector2 pos_o = p_pos - tile_offset;

    Matrix32 rect_part_cell_transform = get_rect_part_cell_transform();
    Vector2 ret_rect_part_cell = rect_part_cell_transform.affine_inverse().xform(pos_o).floor();
    Vector2 ret = Vector2();

    switch(mode) {
        case MODE_HORIZONTAL: {
            ret.x = Math::floor(ret_rect_part_cell.x / 3);
            ret.y = Math::floor(ret_rect_part_cell.y / 2 - 0.5 * (ret.x - 0.5));
        } break;
        case MODE_VERTICAL: {
            ret.y = Math::floor(ret_rect_part_cell.y / 3);
            ret.x = Math::floor(ret_rect_part_cell.x / 2 - 0.5 * (ret.y - 0.5));
        } break;
    }

    if (!p_ignore_orientation) {
        ret = get_oriented_coordinate(ret, orientation, true);
    }

    switch(mode) {
        case MODE_HORIZONTAL: {
            Vector2 ret_world_pos = map_to_world(ret);

            Vector2 pos_in_cell = pos_o - ret_world_pos;
            Vector2 rect_pos_in_cell = get_rect_part_cell_transform().affine_inverse().xform(pos_in_cell).floor();

            if (rect_pos_in_cell.x == 0) {

                if (rect_pos_in_cell.y == 0) {
                    if (pos_in_cell.y < (rect_part_cell_transform[1].y - rect_part_cell_transform[1].y / rect_part_cell_transform[0].x * pos_in_cell.x) ) {
                        ret += get_oriented_coordinate(Vector2(-1, 0), orientation, true);
                    }
                } else {
                    if (pos_in_cell.y - rect_part_cell_transform[1].y > (rect_part_cell_transform[1].y / rect_part_cell_transform[0].x * pos_in_cell.x) ) {
                        ret += get_oriented_coordinate(Vector2(-1, 1), orientation, true);
                    }
                }
            }
        } break;
        case MODE_VERTICAL: {
            Vector2 ret_world_pos = map_to_world(ret);

            Vector2 pos_in_cell = pos_o - ret_world_pos;
            Vector2 rect_pos_in_cell = get_rect_part_cell_transform().affine_inverse().xform(pos_in_cell).floor();

            if (rect_pos_in_cell.y == 0) {
                if (rect_pos_in_cell.x == 0) {
                    if (pos_in_cell.y < (rect_part_cell_transform[1].y - rect_part_cell_transform[1].y / rect_part_cell_transform[0].x * pos_in_cell.x) ) {
                        ret += get_oriented_coordinate(Vector2(0, -1), orientation, true);
                    }
                } else {
                    if (pos_in_cell.y < (rect_part_cell_transform[1].y / rect_part_cell_transform[0].x * (pos_in_cell.x - rect_part_cell_transform[0].x)) ) {
                        ret += get_oriented_coordinate(Vector2(1, -1), orientation, true);
                    }
                }
            }

        } break;
    }

    return ret;
}

Vector2 HexagonalTileMap::get_oriented_coordinate(Vector2 p_coordinate, Orientation p_orientation, bool p_reverse) const{
    if (p_reverse) {
        switch(p_orientation) {
            case DEGREE_60: {
                p_coordinate.x += p_coordinate.y;
                p_coordinate.y -= p_coordinate.x;
            } break;
            case DEGREE_120: {
                p_coordinate.y = -p_coordinate.x-p_coordinate.y;
                p_coordinate.x = -p_coordinate.x-p_coordinate.y;
            } break;
            case DEGREE_180: {
                p_coordinate.x = -p_coordinate.x;
                p_coordinate.y = -p_coordinate.y;
            } break;
            case DEGREE_240: {
                p_coordinate.x = -p_coordinate.x-p_coordinate.y;
                p_coordinate.y = -p_coordinate.x-p_coordinate.y;
            } break;
            case DEGREE_300: {
                p_coordinate.y += p_coordinate.x;
                p_coordinate.x -= p_coordinate.y;
            } break;
            default: {

            } break;
        }
    } else {
        switch(p_orientation) {
            case DEGREE_60: {
                p_coordinate.y += p_coordinate.x;
                p_coordinate.x -= p_coordinate.y;
            } break;
            case DEGREE_120: {
                p_coordinate.x = -p_coordinate.x-p_coordinate.y;
                p_coordinate.y = -p_coordinate.x-p_coordinate.y;
            } break;
            case DEGREE_180: {
                p_coordinate.x = -p_coordinate.x;
                p_coordinate.y = -p_coordinate.y;
            } break;
            case DEGREE_240: {
                p_coordinate.y = -p_coordinate.x-p_coordinate.y;
                p_coordinate.x = -p_coordinate.x-p_coordinate.y;
            } break;
            case DEGREE_300: {
                p_coordinate.x += p_coordinate.y;
                p_coordinate.y -= p_coordinate.x;
            } break;
            default: {

            } break;
        }
    }

    return p_coordinate;
}

void HexagonalTileMap::set_y_sort_mode(bool p_enable) {

    _clear_quadrants();
    y_sort_mode=p_enable;
    VS::get_singleton()->canvas_item_set_sort_children_by_y(get_canvas_item(),y_sort_mode);
    _recreate_quadrants();
    emit_signal("settings_changed");

}

bool HexagonalTileMap::is_y_sort_mode_enabled() const {

    return y_sort_mode;
}

Array HexagonalTileMap::get_used_cells() const {

    Array a;
    a.resize(tile_map.size());
    int i=0;
    for (Map<PosKey,Cell>::Element *E=tile_map.front();E;E=E->next()) {

        Vector2 p (E->key().x,E->key().y);
        a[i++]=p;
    }

    return a;
}

void HexagonalTileMap::set_occluder_light_mask(int p_mask) {

    occluder_light_mask=p_mask;
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        for (Map<PosKey,Quadrant::Occluder>::Element *F=E->get().occluder_instances.front();F;F=F->next()) {
            VisualServer::get_singleton()->canvas_light_occluder_set_light_mask(F->get().id,occluder_light_mask);
        }
    }
}

int HexagonalTileMap::get_occluder_light_mask() const{

    return occluder_light_mask;
}

void HexagonalTileMap::set_light_mask(int p_light_mask) {

    CanvasItem::set_light_mask(p_light_mask);
    for (Map<PosKey,Quadrant>::Element *E=quadrant_map.front();E;E=E->next()) {

        for (List<RID>::Element *F=E->get().canvas_items.front();F;F=F->next()) {
            VisualServer::get_singleton()->canvas_item_set_light_mask(F->get(),get_light_mask());
        }
    }
}

void HexagonalTileMap::_bind_methods() {


    ObjectTypeDB::bind_method(_MD("set_tileset","tileset:TileSet"),&HexagonalTileMap::set_tileset);
    ObjectTypeDB::bind_method(_MD("get_tileset:TileSet"),&HexagonalTileMap::get_tileset);

    ObjectTypeDB::bind_method(_MD("set_mode","mode"),&HexagonalTileMap::set_mode);
    ObjectTypeDB::bind_method(_MD("get_mode"),&HexagonalTileMap::get_mode);

    ObjectTypeDB::bind_method(_MD("set_orientation","orientation"),&HexagonalTileMap::set_orientation);
    ObjectTypeDB::bind_method(_MD("get_orientation"),&HexagonalTileMap::get_orientation);

    ObjectTypeDB::bind_method(_MD("set_height_size","height_size"),&HexagonalTileMap::set_height_size);
    ObjectTypeDB::bind_method(_MD("get_height_size"),&HexagonalTileMap::get_height_size);

    ObjectTypeDB::bind_method(_MD("set_cell_scale_y","scale_y"),&HexagonalTileMap::set_cell_scale_y);
    ObjectTypeDB::bind_method(_MD("get_cell_scale_y"),&HexagonalTileMap::get_cell_scale_y);

    ObjectTypeDB::bind_method(_MD("set_edge_size","edge_size"),&HexagonalTileMap::set_edge_size);
    ObjectTypeDB::bind_method(_MD("get_edge_size"),&HexagonalTileMap::get_edge_size);

    ObjectTypeDB::bind_method(_MD("set_quadrant_size","size"),&HexagonalTileMap::set_quadrant_size);
    ObjectTypeDB::bind_method(_MD("get_quadrant_size"),&HexagonalTileMap::get_quadrant_size);

    ObjectTypeDB::bind_method(_MD("set_modulate","modulate"),&HexagonalTileMap::set_modulate);
    ObjectTypeDB::bind_method(_MD("get_modulate"),&HexagonalTileMap::get_modulate);

    ObjectTypeDB::bind_method(_MD("set_tile_origin","origin"),&HexagonalTileMap::set_tile_origin);
    ObjectTypeDB::bind_method(_MD("get_tile_origin"),&HexagonalTileMap::get_tile_origin);

    ObjectTypeDB::bind_method(_MD("set_tile_offset","offset"),&HexagonalTileMap::set_tile_offset);
    ObjectTypeDB::bind_method(_MD("get_tile_offset"),&HexagonalTileMap::get_tile_offset);

    ObjectTypeDB::bind_method(_MD("set_center_x","enable"),&HexagonalTileMap::set_center_x);
    ObjectTypeDB::bind_method(_MD("get_center_x"),&HexagonalTileMap::get_center_x);

    ObjectTypeDB::bind_method(_MD("set_center_y","enable"),&HexagonalTileMap::set_center_y);
    ObjectTypeDB::bind_method(_MD("get_center_y"),&HexagonalTileMap::get_center_y);

    ObjectTypeDB::bind_method(_MD("set_y_sort_mode","enable"),&HexagonalTileMap::set_y_sort_mode);
    ObjectTypeDB::bind_method(_MD("is_y_sort_mode_enabled"),&HexagonalTileMap::is_y_sort_mode_enabled);

    ObjectTypeDB::bind_method(_MD("set_collision_use_kinematic","use_kinematic"),&HexagonalTileMap::set_collision_use_kinematic);
    ObjectTypeDB::bind_method(_MD("get_collision_use_kinematic"),&HexagonalTileMap::get_collision_use_kinematic);

    ObjectTypeDB::bind_method(_MD("set_collision_layer","mask"),&HexagonalTileMap::set_collision_layer);
    ObjectTypeDB::bind_method(_MD("get_collision_layer"),&HexagonalTileMap::get_collision_layer);

    ObjectTypeDB::bind_method(_MD("set_collision_mask","mask"),&HexagonalTileMap::set_collision_mask);
    ObjectTypeDB::bind_method(_MD("get_collision_mask"),&HexagonalTileMap::get_collision_mask);

    ObjectTypeDB::bind_method(_MD("set_collision_friction","value"),&HexagonalTileMap::set_collision_friction);
    ObjectTypeDB::bind_method(_MD("get_collision_friction"),&HexagonalTileMap::get_collision_friction);

    ObjectTypeDB::bind_method(_MD("set_collision_bounce","value"),&HexagonalTileMap::set_collision_bounce);
    ObjectTypeDB::bind_method(_MD("get_collision_bounce"),&HexagonalTileMap::get_collision_bounce);

    ObjectTypeDB::bind_method(_MD("set_occluder_light_mask","mask"),&HexagonalTileMap::set_occluder_light_mask);
    ObjectTypeDB::bind_method(_MD("get_occluder_light_mask"),&HexagonalTileMap::get_occluder_light_mask);

    ObjectTypeDB::bind_method(_MD("set_cell","x","y","tile","flip_x","flip_y","transpose"),&HexagonalTileMap::set_cell,DEFVAL(false),DEFVAL(false),DEFVAL(false));
    ObjectTypeDB::bind_method(_MD("set_cellv","pos","tile","flip_x","flip_y","transpose"),&HexagonalTileMap::set_cellv,DEFVAL(false),DEFVAL(false),DEFVAL(false));
    ObjectTypeDB::bind_method(_MD("set_cell_side","x","y","tile"),&HexagonalTileMap::set_cell_side);
    ObjectTypeDB::bind_method(_MD("set_cellv_side","pos","tile"),&HexagonalTileMap::set_cellv_side);
    ObjectTypeDB::bind_method(_MD("get_cell","x","y"),&HexagonalTileMap::get_cell);
    ObjectTypeDB::bind_method(_MD("get_cellv","pos"),&HexagonalTileMap::get_cellv);
    ObjectTypeDB::bind_method(_MD("is_cell_x_flipped","x","y"),&HexagonalTileMap::is_cell_x_flipped);
    ObjectTypeDB::bind_method(_MD("is_cell_y_flipped","x","y"),&HexagonalTileMap::is_cell_y_flipped);
    ObjectTypeDB::bind_method(_MD("is_cell_transposed","x","y"),&HexagonalTileMap::is_cell_transposed);

    ObjectTypeDB::bind_method(_MD("clear"),&HexagonalTileMap::clear);

    ObjectTypeDB::bind_method(_MD("get_used_cells"),&HexagonalTileMap::get_used_cells);

    ObjectTypeDB::bind_method(_MD("map_to_world","mappos","ignore_half_ofs"),&HexagonalTileMap::map_to_world,DEFVAL(false));
    ObjectTypeDB::bind_method(_MD("world_to_map","worldpos"),&HexagonalTileMap::world_to_map);

    ObjectTypeDB::bind_method(_MD("_clear_quadrants"),&HexagonalTileMap::_clear_quadrants);
    ObjectTypeDB::bind_method(_MD("_recreate_quadrants"),&HexagonalTileMap::_recreate_quadrants);
    ObjectTypeDB::bind_method(_MD("_update_dirty_quadrants"),&HexagonalTileMap::_update_dirty_quadrants);

    ObjectTypeDB::bind_method(_MD("_set_tile_data"),&HexagonalTileMap::_set_tile_data);
    ObjectTypeDB::bind_method(_MD("_get_tile_data"),&HexagonalTileMap::_get_tile_data);

    ADD_PROPERTY( PropertyInfo(Variant::INT,"mode",PROPERTY_HINT_ENUM,"Horizontal,Vertical"),_SCS("set_mode"),_SCS("get_mode"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"orientation",PROPERTY_HINT_ENUM,"0 degree,60 degrees,120 degrees,180 degrees,240 degrees,300 degrees"),_SCS("set_orientation"),_SCS("get_orientation"));
    ADD_PROPERTY( PropertyInfo(Variant::OBJECT,"tile_set",PROPERTY_HINT_RESOURCE_TYPE,"TileSet"),_SCS("set_tileset"),_SCS("get_tileset"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"cell/edge_size",PROPERTY_HINT_RANGE,"1,8192,1"),_SCS("set_edge_size"),_SCS("get_edge_size"));
    ADD_PROPERTY( PropertyInfo(Variant::REAL,"cell/cell_scale_y",PROPERTY_HINT_RANGE,"0.01,1,0.01"),_SCS("set_cell_scale_y"),_SCS("get_cell_scale_y"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"cell/quadrant_size",PROPERTY_HINT_RANGE,"1,128,1"),_SCS("set_quadrant_size"),_SCS("get_quadrant_size"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"cell/height_size",PROPERTY_HINT_RANGE,"-8192,8192,1"),_SCS("set_height_size"),_SCS("get_height_size"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"cell/tile_origin",PROPERTY_HINT_ENUM,"Top Left,Center"),_SCS("set_tile_origin"),_SCS("get_tile_origin"));
    ADD_PROPERTY( PropertyInfo(Variant::VECTOR2,"cell/tile_offset"), _SCS("set_tile_offset"),_SCS("get_tile_offset"));
    ADD_PROPERTY( PropertyInfo(Variant::BOOL,"cell/y_sort"),_SCS("set_y_sort_mode"),_SCS("is_y_sort_mode_enabled"));
    ADD_PROPERTY( PropertyInfo( Variant::COLOR, "cell/modulate"), _SCS("set_modulate"),_SCS("get_modulate"));
    ADD_PROPERTY( PropertyInfo(Variant::BOOL,"collision/use_kinematic",PROPERTY_HINT_NONE,""),_SCS("set_collision_use_kinematic"),_SCS("get_collision_use_kinematic"));
    ADD_PROPERTY( PropertyInfo(Variant::REAL,"collision/friction",PROPERTY_HINT_RANGE,"0,1,0.01"),_SCS("set_collision_friction"),_SCS("get_collision_friction"));
    ADD_PROPERTY( PropertyInfo(Variant::REAL,"collision/bounce",PROPERTY_HINT_RANGE,"0,1,0.01"),_SCS("set_collision_bounce"),_SCS("get_collision_bounce"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"collision/layers",PROPERTY_HINT_ALL_FLAGS),_SCS("set_collision_layer"),_SCS("get_collision_layer"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"collision/mask",PROPERTY_HINT_ALL_FLAGS),_SCS("set_collision_mask"),_SCS("get_collision_mask"));
    ADD_PROPERTY( PropertyInfo(Variant::INT,"occluder/light_mask",PROPERTY_HINT_ALL_FLAGS),_SCS("set_occluder_light_mask"),_SCS("get_occluder_light_mask"));

    ADD_PROPERTY( PropertyInfo(Variant::OBJECT,"tile_data",PROPERTY_HINT_NONE,"",PROPERTY_USAGE_NOEDITOR),_SCS("_set_tile_data"),_SCS("_get_tile_data"));

    ADD_SIGNAL(MethodInfo("settings_changed"));

    BIND_CONSTANT( INVALID_CELL );
    BIND_CONSTANT( MODE_HORIZONTAL );
    BIND_CONSTANT( MODE_VERTICAL );
    BIND_CONSTANT( DEGREE_0 );
    BIND_CONSTANT( DEGREE_60 );
    BIND_CONSTANT( DEGREE_120 );
    BIND_CONSTANT( DEGREE_180 );
    BIND_CONSTANT( DEGREE_240 );
    BIND_CONSTANT( DEGREE_300 );
    BIND_CONSTANT( TILE_ORIGIN_TOP_LEFT );
    BIND_CONSTANT( TILE_ORIGIN_CENTER );

}

HexagonalTileMap::HexagonalTileMap() {

    rect_cache_dirty=true;
    pending_update=false;
    quadrant_order_dirty=false;
    quadrant_size=16;
    edge_size=64;
    height_size=16;
    cell_scale_y=1;
    modulate=Color(1,1,1,1);
    center_x=false;
    center_y=false;
    collision_layer=1;
    collision_mask=1;
    friction=1;
    bounce=0;
    mode=MODE_VERTICAL;
    orientation=DEGREE_0;
    use_kinematic=false;
    navigation=NULL;
    y_sort_mode=false;
    occluder_light_mask=1;

    fp_adjust=0.00001;
    tile_origin=TILE_ORIGIN_TOP_LEFT;
    tile_offset.x = 0;
}

HexagonalTileMap::~HexagonalTileMap() {

    clear();
}
